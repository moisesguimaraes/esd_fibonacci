#include <stdio.h>

unsigned long fibonacci(unsigned long n) {
    if (n == 0 || n == 1)
        return n;
        
    return fibonacci(n-1) + fibonacci(n-2);
}

int main (int argc, char** argv) {
    int n = atoi(argv[1]);
    
    printf("%lu\n", fibonacci(n));
    
    return 0;
}